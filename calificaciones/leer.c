#include <stdio.h>
#include <string.h>   // por el strlen (string length)

int main(){
	char alumnos[1000][80];
	int calis[1000];
	int i,l;
	// el archivo de datos tiene 1000 nombres (1 por renglon)
	// y luego 1000 calificaciones del 0 al 10 (una por renglon)
	for(i=0;i<1000; i++){
		fgets(alumnos[i],80,stdin);
		l=strlen(alumnos[i]);
		if(l>0)
			alumnos[i][l-1]=0;
	}

	for(i=0; i<1000; i++){
		scanf("%d", &calis[i]);
	}

	for(i=0;i<1000; i++){
		printf("%s: %d\n", alumnos[i], calis[i]);
	}
}

