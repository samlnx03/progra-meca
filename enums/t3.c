#include <stdio.h>
//
// una enumeracion es un tipo de dato definido por el usuario
// define a un conjunto de valores relacionados a un mismo ámbito

enum dia_semana {Domingo, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado};
//  dia_semana es global y se puede usar en cualquier funcion

void fun(enum dia_semana d){
	printf("día de la semana: %d\n",d); 
}

int main(){
	enum dia_semana hoy;
	hoy=Domingo; printf("Domingo: ");
	fun(hoy);
	hoy++;
	printf("Siguiente al Domingo es el día %d de la semana\n",hoy); 

	hoy=Sabado; printf("Sabado: ");
	fun(hoy);
	hoy++;
	printf("Siguiente al Sabado es el día %d de la semana\n",hoy); // 3
	printf("NO SE CHECAN LIMITES\n");
}
