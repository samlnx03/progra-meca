#include <stdio.h>
//
// una enumeracion es un tipo de dato definido por el usuario
// define a un conjunto de valores relacionados a un mismo ámbito

int main(){
	enum dia_semana {Domingo, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado};
	//  dia_semana solo se conoce dentro de main y bloques de codigo interno
	// por default el primer valor usa el 0 para su representacion,
	// el 2o valor usa el 1 y asi suscesivamente
	enum dia_semana hoy;
	hoy=Domingo;
	printf("Hoy es el día %d de la semana\n",hoy);
}
