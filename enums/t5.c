#include <stdio.h>
//
// una enumeracion es un tipo de dato definido por el usuario
// define a un conjunto de valores relacionados a un mismo ámbito

//enum dia_semana {Domingo, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado};
//  dia_semana es global y se puede usar en cualquier funcion

//typedef enum dia_semana Dia_semana;
// ya se puede usar Dia_semana como si fuera un tipo de datos como int o char

// una forma mas compacta:
typedef enum {Domingo, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado} Dia_semana;

// typedef int entero;  // un alias para int

void fun(Dia_semana d){
	printf("día de la semana: %d\n",d); 
}

int main(){
	Dia_semana hoy;
	// ya se puede usar Dia_semana como si fuera un tipo de datos como int o char

	hoy=Domingo; printf("Domingo: ");
	fun(hoy);
	hoy++;
	printf("Siguiente al Domingo es el día %d de la semana\n",hoy); 

	hoy=Sabado; printf("Sabado: ");
	fun(hoy);
	hoy++;
	printf("Siguiente al Sabado es el día %d de la semana\n",hoy); // 3
	printf("NO SE CHECAN LIMITES\n");
}
