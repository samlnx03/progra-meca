#include <stdio.h>
//
// una enumeracion es un tipo de dato definido por el usuario
// define a un conjunto de valores relacionados a un mismo ámbito

int main(){
	enum dia_semana {Domingo=3, Lunes, Martes, Miercoles=10, Jueves, Viernes, Sabado};
	//  dia_semana solo se conoce dentro de main y bloques de codigo interno
	// por default el primer valor usa el 0 para su representacion,
	// el 2o valor usa el 1 y asi suscesivamente
	//
	// se puede indicar un valor entero para cada valor de la enumeracion
	// si los subsiguientes no se especifican se usa el siguiente entero
	enum dia_semana hoy;
	hoy=Domingo;
	printf("Domingo es el día %d de la semana\n",hoy); // 3
	hoy=Lunes;
	printf("Lunes es el día %d de la semana\n",hoy);   // 4
	hoy=Jueves;
	printf("Jueves es el día %d de la semana\n",hoy);  // 11  
}
