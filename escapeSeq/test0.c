/*
paginas con informacion relacionada

resumen rapido
https://shiroyasha.svbtle.com/escape-sequences-a-quick-guide-1

lista de codigos de escape
http://ascii-table.com/ansi-escape-sequences-vt-100.php

"\e[3A" # up - moves 3 lines up
"\e[6B" # down - moves 6 lines down
"\e[2C" # forward - moves 2 characters forward
"\e[1D" # backward - moves 1 character backward

"\e[2E" # move to the beginning of 2 lines down 
"\e[4F" # move to the beginning of 4 lines up

"\e[16F" # move to 16th column

"\e[3;9G" # moves cursor to the 3rd row 9th column

"\e[?25h" # show cursor
"\e[?25l" # hide cursor

*/

#include <stdio.h>

#define CLEAR "\e[2J\e[H"
#define GOTORC "\e[%d;%df"
#define HIDECURSOR "\e[?25l"
#define SHOWCURSOR "\e[?25h"

void draw(int r, int c){
	printf(GOTORC,r,c); // goto row,col
	printf(". .");
	printf(GOTORC,r+1,c); // goto row,col
	printf(" v ");
	return;
}
void erase(int r, int c){
	printf(GOTORC,r,c); // goto row,col
	printf("   ");
	printf(GOTORC,r+1,c); // goto row,col
	printf("   ");
	return;
}

void draw1(int r, int c){
		printf(GOTORC,r,c); // goto row,col
		printf("()-()");
		printf(GOTORC,r+1,c+1); // goto row,col
		printf("\\\"/");
		printf(GOTORC,r+2,c+2); // goto row,col
		printf("`");
		/*
()-()   
 \"/ 
  `


	             \_/
		        '-0-'
		        --0--
		        .-0-.
			*/
}
void erase1(int r, int c){
		/*
		printf(GOTORC,r-1,c-1); // goto row,col
		printf("   ");
		printf(GOTORC,r,c); // goto row,col
		printf(" ");
		printf(GOTORC,r+1,c-1); // goto row,col
		printf("   ");
		*/
		printf(GOTORC,r,c); // goto row,col
		printf("     ");
		printf(GOTORC,r+1,c+1); // goto row,col
		printf("   ");
		printf(GOTORC,r+2,c+2); // goto row,col
		printf(" ");
}

int main()
{
		int i;
		printf(CLEAR); // clear screen
		printf(HIDECURSOR);
		for(i=0; i<10; i++){
				draw(i+2,i+2);
				fflush(stdout);
				sleep(1);
				erase(i+2,i+2);
				fflush(stdout);
		}
		printf(SHOWCURSOR);

}
