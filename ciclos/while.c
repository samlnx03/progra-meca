#include <stdio.h>

int main(){
	int i;
	// ciclos for, contar de 1 a 25
	//	for(i=1;  esta la la inicializacion 
			//(solo se ejecuta una vez, al inicio)
	// for (i=1; i<26  esta es la condicion de paro del ciclo 
			//(mientras i sea menor que 26)
	// for(i=1; i<26; i++) i++ es la accion ultima del ciclo
		//cuando termine todas las sentencias del ciclo hara i=i+1
	//for(i=1; i<26; i++){
	//for(i=1; i<26; ){
	i=0;
	/*
	for(;; ){
			printf("%d\n",i);
			i=i+1;
			if(i>5) break;
	}*/
	while(i<26)
	{
			printf("%d\n",i);
			i=i+1;
	}

	return 0;
}
