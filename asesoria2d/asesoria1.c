#include <stdio.h>

int main(){
	char v1;
	char a1[10]="Hola";

	int i1;
	int ai[10]={5,10,3};
	int a2d[2][3] = {
				{1,2,3},
				{4,5,6}
			};

	int i;
	i=0;
	printf("caracter %d de a1=%c\n", i, a1[i]);
	v1=a1[i];
	printf("%c tiene el codigo %d\n",v1,v1);

	printf("Enteros\n");
	printf("el elemento %d del arreglo ai es %d\n",i,ai[i]);

	printf("Dos dimensiones\n");
	printf("el elemento [%d][1] del arreglo a2d es %d\n",i,a2d[i][1]);
	
}
