#include <stdio.h>
#include "fontdef.h"


int main(){
	char v1;
	char a1[10]="Hola!";

	int i,j,k;
	unsigned char mask;
	unsigned char asteriscos;
	for(i=0;(v1=a1[i])!='\0';i++){
		printf("caracter %c\n",v1);
		j=v1-' '+1;
		printf("\tposicion en el arreglo de definicion de font %d\n",j);
		printf("\t");
		for (k=0; k<8; k++)
			printf("%2x ",MyFONT6x8[j][k]);
		printf("\n");
		  // 	binario 	1000 0000   80 hexadecimal
		  //   mask=mask>>1	0100 0000   40 hexadecimal
		  //   and del valor de definicio de font y las mask
		 // 	 me da la decicion si pintar o no el *
		for (k=0; k<8; k++){
			mask=0x80;
			asteriscos=MyFONT6x8[j][k];
			printf("\t%2x ",asteriscos);
			while(mask!=0){
				if(asteriscos&mask)
					printf("*");
				else
					printf(" ");
				mask=mask>>1;
			}
			printf("\n");	
		}
		printf("\n");
		
	}
}
