// numero complejos usando estructuras

#include <stdio.h>

typedef struct{
		float r;
		float i;
} Complejo;

void sumac(Complejo x, Complejo y, Complejo *r);
void imprime(Complejo n);

int main(){
	Complejo n1, n2,r;
	n1.r=5; n1.i=-2;
	n2.r=-1; n2.i=3;
	sumac(n1,n2,&r);
	imprime(r);
}

// suma de dos complejos, r recibe el resultado
void sumac(Complejo x, Complejo y, Complejo *r){
	r->r=x.r+y.r;
	r->i=x.i+y.i;
}

// impresion de un complejo
void imprime(Complejo n){
	printf("%f %+fi\n",n.r, n.i);
}
