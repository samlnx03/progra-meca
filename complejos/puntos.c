#include <stdio.h>
#include <math.h>

typedef  struct {
	int x,y;
} Punto;

float distancia(Punto p1, Punto p2);

int main(){
	Punto A, B;
	printf("coordnadas x,y de A: ");
	scanf("%i,%i",&A.x, &A.y);
	
	printf("coordnadas x,y de B: ");
	scanf("%i,%i",&B.x, &B.y);

	printf("La distancia de A a B es de %f\n", distancia(A,B));
	return 0;
}

float distancia(Punto p1, Punto p2){
	float h;
	h=sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
	return h;
}

