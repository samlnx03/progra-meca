
#include <stdio.h>
#include <math.h>

typedef  struct {
	int x,y;
} Punto;

typedef struct {
	Punto p1, p2;
} Rectangulo;


int area(Rectangulo x);

int main(){
	Rectangulo R;
	printf("coordenadas P1(x,y) del Rectangulo: ");
	scanf("%i,%i",&R.p1.x, &R.p1.y);
	
	printf("coordenadas P2(x,y) del Rectangulo: ");
	scanf("%i,%i",&R.p2.x, &R.p2.y);

	printf("El area del rectangulo es %d\n", area(R));
	return 0;
}

int area(Rectangulo x){
	int a;
	a=abs((x.p1.x-x.p2.x) * (x.p1.y-x.p2.y));
	return a;
}

