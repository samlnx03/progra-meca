// fig3.c
// se usa una estructura para representar a un pixel
// se usa un arreglo de memoria como buffer de la imagen

#include <stdio.h>

#define ANCHO 100
#define ALTO 200


typedef struct{
	unsigned char rojo;
	unsigned char verde;
	unsigned char azul;
} Pixel;

void fondo(Pixel b[][ANCHO], unsigned char R, unsigned char G, unsigned char B);
void cabecera(int w, int h);
void imprime_buffer(Pixel b[][ANCHO]);

int main(){
	Pixel buffer[ALTO][ANCHO];
	int i,j;

	// colorear el fondo de un color
	fondo(buffer,255,255,255);

	

	// margen inferior
	for(i=ALTO-10; i<=ALTO-1; i++){
		for(j=0; j<=ANCHO-1; j++){
			buffer[i][j].rojo=255;
			buffer[i][j].verde=0;
			buffer[i][j].azul=0;
		}
	}
	
	// margen superior
	for(i=0; i<=9; i++){
		for(j=0; j<=ANCHO-1; j++){
			buffer[i][j].rojo=255;
			buffer[i][j].verde=0;
			buffer[i][j].azul=0;
		}
		//printf("\n# se imprimio renglon %d\n",i);
	}

	// laterales
	for(i=10; i<=ALTO-10-1; i++){
		for(j=0; j<=9; j++){
			buffer[i][j].rojo=255;
			buffer[i][j].verde=0;
			buffer[i][j].azul=0;

			buffer[i][ANCHO-10+j].rojo=255;
			buffer[i][ANCHO-10+j].verde=0;
			buffer[i][ANCHO-10+j].azul=0;
		}
	}

	cabecera(ANCHO, ALTO);
	imprime_buffer(buffer);


	/*
	// laterales
	for(i=11; i<=Alto-10; i++){
		for(j=1; j<=10; j++){
			printf("255 0 0 ");
		}
		printf("#Se imprimio lateral izq\n");
		// blancos
		for(j=11; j<=Ancho-10; j++){
			printf("255 255 255 ");
		}
		printf("#se imprimeron los blancos\n");
		for(j=Ancho-10+1; j<=Ancho; j++){
			printf("255 0 0 ");
		}
		printf("#Se imprimio lateral der\n");
		//printf("\n");
	}
	*/
	return 0;
}

void cabecera(int w, int h){
	printf("P3\n");
	printf("%d %d\n",w,h);
	printf("255\n");
}

void fondo(Pixel buffer[][ANCHO], unsigned char R, unsigned char G, unsigned char B){
	int r,c; // renglo0n columna
	for(r=0; r<ALTO; r++){
		for(c=0; c<ANCHO; c++){
			buffer[r][c].rojo=R;
			buffer[r][c].verde=G;
			buffer[r][c].azul=B;
		}
	}
}

void imprime_buffer(Pixel b[][ANCHO]){
	int r,c; // renglo0n columna
	for(r=0; r<ALTO; r++){
		for(c=0; c<ANCHO; c++){
			printf("%d %d %d ", b[r][c].rojo, b[r][c].verde, b[r][c].azul);
		}
		printf("\n");
	}
}

