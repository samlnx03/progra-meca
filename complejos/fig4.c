// fig3.c
// se usa una estructura para representar a un pixel
// se usa un arreglo de memoria como buffer de la imagen

// se renombra Pixel a Color
// y se usa tambien para el fondo 

#include <stdio.h>

#define ANCHO 100
#define ALTO 200


typedef struct{
	unsigned char rojo;
	unsigned char verde;
	unsigned char azul;
} Color;

void fondo(Color b[][ANCHO], Color f);
void cabecera(int w, int h);
void imprime_buffer(Color b[][ANCHO]);

int main(){
	Color buffer[ALTO][ANCHO];
	int i,j;

	// colorear el fondo de un color
	Color f;
	f.rojo=255; f.verde=255; f.azul=255;
	fondo(buffer,f);

	

	// margen inferior
	for(i=ALTO-10; i<=ALTO-1; i++){
		for(j=0; j<=ANCHO-1; j++){
			buffer[i][j].rojo=255;
			buffer[i][j].verde=0;
			buffer[i][j].azul=0;
		}
	}
	
	// margen superior
	for(i=0; i<=9; i++){
		for(j=0; j<=ANCHO-1; j++){
			buffer[i][j].rojo=255;
			buffer[i][j].verde=0;
			buffer[i][j].azul=0;
		}
		//printf("\n# se imprimio renglon %d\n",i);
	}

	// laterales
	for(i=10; i<=ALTO-10-1; i++){
		for(j=0; j<=9; j++){
			buffer[i][j].rojo=255;
			buffer[i][j].verde=0;
			buffer[i][j].azul=0;

			buffer[i][ANCHO-10+j].rojo=255;
			buffer[i][ANCHO-10+j].verde=0;
			buffer[i][ANCHO-10+j].azul=0;
		}
	}

	cabecera(ANCHO, ALTO);
	imprime_buffer(buffer);
	return 0;
}

void cabecera(int w, int h){
	printf("P3\n");
	printf("%d %d\n",w,h);
	printf("255\n");
}

void fondo(Color buffer[][ANCHO], Color f){
	int r,c; // renglo0n columna
	for(r=0; r<ALTO; r++){
		for(c=0; c<ANCHO; c++){
			buffer[r][c].rojo=f.rojo;
			buffer[r][c].verde=f.verde;
			buffer[r][c].azul=f.azul;
		}
	}
}

void imprime_buffer(Color b[][ANCHO]){
	int r,c; // renglo0n columna
	for(r=0; r<ALTO; r++){
		for(c=0; c<ANCHO; c++){
			printf("%d %d %d ", b[r][c].rojo, b[r][c].verde, b[r][c].azul);
		}
		printf("\n");
	}
}

