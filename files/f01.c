// manejo de archivos en c

// primer tipo: archivos de texto, escribir registros con dos columnas
// escritura

#include <stdio.h>

int main(){
	FILE *f;
	char filename[]="2write.txt";  
	//char filename[]="/tmp/2write.txt";

	int i;
	char numeros[][7]={"Cero", "uno", "dos", "tres", "cuatro", "cinco",
		"seis", "siete", "ocho", "nueve"};
	f=fopen(filename, "w");  //abrir para escritura, si existe se trunca
	if(f==NULL){
		printf("no se pudo abrir archivo\n");
		return -1;
	}
	for(i=0; i<10; i++){
		fprintf(f,"%d %s\n",i,numeros[i]);
	}
	fclose(f);
	return 0;
}
