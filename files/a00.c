// leer archov csv de accidentes

// http://www.beta.inegi.org.mx/contenidos/temas/economia/accidentes/tabulados/accidentes_4.xlsx

// separador de campos: ;

/*
// campos
	clave del estado de la republica
	estado de la republica
	total accidentes
	colision con vehiculo
	colision con peaton
	colision con objeto fijo
	colision con motociclista
	otros
*/

#include <stdio.h>

int main(){
	FILE *f;
	char filename[]="accidentes.csv";  

	int id, te, cv, cp, cof, cm, otro;
	char estado[50];
	f=fopen(filename, "r");  //abrir para lectura
	if(f==NULL){
		printf("no se pudo abrir archivo\n");
		return -1;
	}
	//for(i=0; i<10; i++){
	while(fscanf(f,"%d;%[^;];%d;%d;%d;%d;%d;%d",&id,estado,&te, &cv, &cp, &cof, &cm, &otro)!=EOF)
		printf("%d %s %d %d %d %d %d %d\n",id, estado, te, cv, cp, cof, cm, otro);
	
	fclose(f);
	return 0;
}
