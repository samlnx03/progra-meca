// manejo de archivos en c

// primer tipo: archivos de texto, leer registros con dos columnas
// lectura

#include <stdio.h>

int main(){
	FILE *f;
	char filename[]="2write.txt";  
	//char filename[]="/tmp/2write.txt";

	int i,n;
	char s[7];
	f=fopen(filename, "r");  //abrir para lectura
	if(f==NULL){
		printf("no se pudo abrir archivo\n");
		return -1;
	}
	for(i=0; i<10; i++){
		fscanf(f,"%d %s\n",&n,s);
		printf("%d.-%s\n",n,s);
	}
	fclose(f);
	return 0;
}
