// manejo de archivos en c

// primer tipo: archivos de texto
// escritura

#include <stdio.h>

int main(){
	FILE *f;
	//char filename[]="2write.txt";  
	char filename[]="/tmp/2write.txt";
	f=fopen(filename, "w");  //abrir para escritura, si existe se trunca
	if(f==NULL){
		printf("no se pudo abrir archivo\n");
		return -1;
	}
	fprintf(f,"Escribiendo en el archivo\n");
	fclose(f);
	return 0;
}
