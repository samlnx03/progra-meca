#include <stdio.h>

int main(){
	unsigned char F[]={0x78, 0x88, 0x80, 0x80, 0x98, 0x88, 0x78, 0x00};
	int i,j;
	unsigned char p;
	printf("Datos para definir la letra G\n");
	for(i=0; i<8; i++){
		printf("decimal %d = %x hexadecimal\n", F[i], F[i]);
	}

	// prueba de corrimientos
	printf("corrimientos de 1 bit para enmascarar pixel\n");
	i=0x80;
	while(i>0){
		printf("decimal %d = %x hexadecimal\n",i,i);
		i=i>>1;
	}
	printf("investigar pixeles\n");
	for(j=0; j<8; j++){
		i=0x80;
		while(i>0){
			p=F[j] & i;
			if(p!=0)
				//printf("pintar en la pos. %x\n",i);
				printf("*");
			else
				//printf("No pintar en la pos. %x\n",i);
				printf(" ");

			//printf("%d %i\n",i,i);
			i=i>>1;
		}
		printf("\n");
	}
	
	return 0;
}

