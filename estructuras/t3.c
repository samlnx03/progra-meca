#include <stdio.h>

struct alumno{
	int num;
	int promDepartamental;
	int promParciales;
};

void imprime(struct alumno a){
	printf("Alumno %d, P.depart: %d, P.parciales: %d\n", a.num, a.promDepartamental, a.promParciales);
	return;
}

main(){
	struct alumno a[3];		// un arreglo de elementos de estructuras alumno
	struct alumno *pa;
	pa=&a[0];   // o se podia usar simplemente pa=a;
	pa->num=1;
	pa->promDepartamental=6;
	pa->promParciales=10;
	imprime(a[0]);
	imprime(*pa);  // es lo mismo que la linea anterior
}

