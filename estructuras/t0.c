#include <stdio.h>

/* la estructura es un tipo de dato definido por el usuario
	sirve para aglutinar a multiples variables incluso de distinto tipo
*/

struct alumno{
	int num;
	int promDepartamental;
	int promParciales;
};

void imprime(struct alumno a){
	printf("Alumno %d, P.depart: %d, P.parciales: %d\n", a.num, a.promDepartamental, a.promParciales);
	return;
}

main(){
	struct alumno a1;
	a1.num=1;
	a1.promDepartamental=6;
	a1.promParciales=10;
	imprime(a1);
}

