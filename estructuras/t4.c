#include <stdio.h>

typedef struct {
	int num;
	int promDepartamental;
	int promParciales;
} Alumno;

void imprime(Alumno a){
	printf("Alumno %d, P.depart: %d, P.parciales: %d\n", a.num, a.promDepartamental, a.promParciales);
	return;
}

main(){
	Alumno a[3];		// un arreglo de elementos de estructuras alumno
	Alumno *pa;
	pa=&a[0];   // o se podia usar simplemente pa=a;
	pa->num=1;
	pa->promDepartamental=6;
	pa->promParciales=10;
	imprime(a[0]);
	imprime(*pa);  // es lo mismo que la linea anterior
}

