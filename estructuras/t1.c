#include <stdio.h>

/* la estructura es un tipo de dato definido por el usuario
	sirve para aglutinar a multiples variables incluso de distinto tipo
*/

struct alumno{
	int num;
	int promDepartamental;
	int promParciales;
};

void imprime(struct alumno a){
	printf("Alumno %d, P.depart: %d, P.parciales: %d\n", a.num, a.promDepartamental, a.promParciales);
	return;
}

main(){
	struct alumno a1;
	struct alumno *pa;  // un apuntador al tipo de dato estructura alumno
	pa=&a1;				// el apuntador apunta a a1
	pa->num=1;					// acceso con el apuntador
	pa->promDepartamental=6;
	pa->promParciales=10;
	imprime(a1);
}

