/*
   uso de scanf para leer mas de una variable desde la entrada estandar
*/

#include <stdio.h>

int main(void)
{
		int i,j;
		char c;
		printf("introducir 2 valores enteros, separados por *,+,- o / ");
		scanf("%d%c%d",&i,&c,&j);
		printf("los valores leidos: %d %c %d\n",i,c,j);


		return 0;
}

