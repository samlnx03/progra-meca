/*
   uso de scanf para leer mas de una variable desde la entrada estandar
*/

#include <stdio.h>

int main(void)
{
		int i,j;
		printf("introducir 2 valores enteros, separados por espacio ");
		scanf("%d %d",&i,&j);
		printf("los valores leidos: %d %d\n",i,j);

		printf("introducir 2 valores enteros, separados por coma ");
		scanf("%d,%d",&i,&j);  // note la coma entre los %d,%d
		printf("los valores leidos: %d %d\n",i,j);

		printf("introducir 2 valores enteros, separados por / (ejemplo 23/12) ");
		scanf("%d/%d",&i,&j);  // note la / entre los %d/%d
		printf("los valores leidos: %d %d\n",i,j);

		return 0;
}

