/*
   uso de scanf para leer datos desde un archivo con redireccion de la
   entrada estandar y deteccion del fin de los datos con EOF
uso:	./a.out < datos3.txt

donde datos3.txt tiene los datos en el formato que se esperan
en este caso datos3.txt tiene lo siguiente:

23
12
13
15
3
8

 */

#include <stdio.h>

int main(void)
{
		int i, ndatos;
		//scanf regresa el numero de variables leidas 
		//	o EOF al terminarse los datos en la entrada

		while((ndatos=scanf("%d",&i))!=EOF)
			printf("valor leido: %d\n",i);

		return 0;
}

