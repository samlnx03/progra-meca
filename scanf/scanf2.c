/*
   uso de scanf para leer datos desde un archivo con redireccion de la
   entrada estandar
uso:	./a.out < datos.txt

donde datos.txt tiene los datos en el formato que se esperan
en este caso datos.txt tiene lo siguiente:

23 12
13,15
3/8

 */

#include <stdio.h>

int main(void)
{
		int i,j;
		//printf("introducir 2 valores enteros, separados por espacio ");
		scanf("%d %d",&i,&j);
		printf("los valores leidos: %d %d\n",i,j);

		//printf("introducir 2 valores enteros, separados por coma ");
		scanf("%d,%d",&i,&j);  // note la coma entre los %d,%d
		printf("los valores leidos: %d %d\n",i,j);

		//printf("introducir 2 valores enteros, separados por / (ejemplo 23/12) ");
		scanf("%d/%d",&i,&j);  // note la / entre los %d/%d
		printf("los valores leidos: %d %d\n",i,j);

		return 0;
}

