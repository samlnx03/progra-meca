#include <stdio.h>

int main(){
	int dia;
	printf("dar el numero del dia de la semana: ");
	scanf("%d",&dia);
	if(dia==0){
		printf("el %d corresponde al Domingo\n",dia);
	}
	else if(dia==1){
		printf("el %d corresponde al Lunes\n",dia);
	}
	else if(dia==2){
		printf("el %d corresponde al Martes\n",dia);
	}
	else if(dia==3){
		printf("el %d corresponde al Miercoles\n",dia);
	}
	else if(dia==4){
		printf("el %d corresponde al Jueves\n",dia);
	}
	else if(dia==5){
		printf("el %d corresponde al Viernes\n",dia);
	}
	else if(dia==6){
		printf("el %d corresponde al Sabado\n",dia);
	}
	/*
	// el else no es obligatorio, si no esta y ningun else if se cumple, entonce
	//  no se realiza ninguna accion.
	else {
		printf("el %d NO corresponde a ningun dia\n",dia);
	}
	*/
	return 0;
}
