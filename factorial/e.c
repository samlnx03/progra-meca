#include <stdio.h>

long factorial(int n);

int main(){
	int n,i;
	double e=1;
	printf("numero de terminos para el numero e ");
	scanf("%d",&n);
	for(i=1;i<=n; i++){
		e=e+1.0/factorial(i);  // e=(double)1/factorial(i);
	}
	printf("numero e con %d terminos es %lf\n",n, e);
	return 0;
}

long factorial(int n){
	unsigned long f=n;
	while(n>1){
		n=n-1;
		f=f*n;
	}
	return f;
}
