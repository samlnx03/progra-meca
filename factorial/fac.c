#include <stdio.h>

long factorial(int n);

int main(){
	int n;
	printf("numero del cual calcular el factorial ");
	scanf("%d",&n);
	printf("factorial de %d es %lu\n",n, factorial(n));
	return 0;
}

long factorial(int n){
	unsigned long f=n;
	while(n>1){
		n=n-1;
		f=f*n;
	}
	return f;
}
