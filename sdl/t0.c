// compilar con gcc t0.c -lSDL2

#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>

#define ANCHO 500	// ancho de la ventana
#define ALTO 500

SDL_Window *window;
SDL_Renderer *renderer;

void esperaHastaTerminar();
void inicializa();
void terminar();

int main(){
	inicializa();
	//
	// actividades  principales
	// poner color gris y limpiar pantalla
	SDL_SetRenderDrawColor(renderer, 127,127,127,255); // r,g,b, alpha
	SDL_RenderClear(renderer);
	
	// dibujar un cuadro
	SDL_SetRenderDrawColor(renderer, 255,0,0,255); //rojo
	SDL_Rect rect;
	rect.x=ANCHO/4;
	rect.y=ALTO/4;
	rect.w=ANCHO/2;
	rect.h=ALTO/2;
	SDL_RenderFillRect(renderer, &rect);
	// y mostrar lo realizado
	SDL_RenderPresent(renderer);

	// y ...
	esperaHastaTerminar();
	terminar();
	return 0;
}

void inicializa(){
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Titulo", 10,20, ANCHO, ALTO,SDL_WINDOW_RESIZABLE);
	renderer=SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void terminar(){
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void esperaHastaTerminar(){
	SDL_bool running=SDL_TRUE;
	while(running){
		SDL_Event event;
		while(SDL_PollEvent(&event) != 0){
			if(event.type == SDL_QUIT)
			{
				running=SDL_FALSE;
				break;
			}
		}
	}
}

