/*
uso de variables 
*/

#include <stdio.h>
int main (){	// esta es la funcion principal
	int i,j, k=0;  // 3 variable enteras, una inicializa en 0
	float salario;
	i=3; j=-2;  // 2 sentencia en mismo renglon pero cada una con ;
	salario=80.94;
	printf("i:%d, j:%d, k:%d, salario:%.2f\n",i,j,k,salario);
	// los %d en printf sirven para imprimier valores decimales
	// %f es para flotantes
}

