// leer datos de la entrada estandard
// scanf

#include <stdio.h>

int main(){
	int x, y;
	float c;
	printf("dar el valor de x: ");
	scanf("%d", &x);  // se debe agregar el & antes de la variable
	// scanf: entre comillas el tipo de variable a leer
	printf("dar el valor de y: ");
	scanf("%d", &y);  // se debe agregar el & antes de la variable

	c=x/y;		// division entre enteros -> resultado entero
	printf("x:%d y:%d c:%f\n",x,y,c);

	// TYPE CASTING de la x
	c=(float)x/y;		// division floatante / entero -> flotante
	printf("x:%d y:%d c:%f\n",x,y,c);
}

