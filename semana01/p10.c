// leer datos de la entrada estandard redireccionada a un archivo de texto
// 
// ejecutar con:     ./a.out < datos.txt


#include <stdio.h>

int main(){
	int x, y;
	float c;
	printf("dar el valores (separados por espacio) de x y: ");
	scanf("%d %d", &x, &y);  // se debe agregar el & antes de la variables
	// scanf leera dos valores separados por espacio

	c=x/y;		// division entre enteros -> resultado entero
	printf("x:%d y:%d c:%f\n",x,y,c);

	// TYPE CASTING de la x
	c=(float)x/y;		// division floatante / entero -> flotante
	printf("x:%d y:%d c:%f\n",x,y,c);
}

