#include <stdio.h>

int main(){
	int x=5, y=2;
	float c;
	c=x/y;		// division entre enteros -> resultado entero
	printf("x:%d y:%d c:%f\n",x,y,c);

	// TYPE CASTING de la x
	c=(float)x/y;		// division floatante / entero -> flotante
	printf("x:%d y:%d c:%f\n",x,y,c);
}

