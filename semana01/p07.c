/*
uso de variables y operadores aritmeticos 
*/

#include <stdio.h>
int main (){	// esta es la funcion principal
	int i,j, k=0;  // 3 variable enteras, una inicializa en 0
	float salario;
	double peso;
	float ingreso_mes;
	float ingreso_semana;
	i=3; j=-2;  // 2 sentencia en mismo renglon pero cada una con ;
	salario=80.94;
	peso=54.597;
	ingreso_mes=salario*30;
	ingreso_semana=ingreso_mes/4;
	printf("i:%d, j:%d, k:%d, salario:%.2f peso:%e\n",i,j,k,salario,peso);
	// los %d en printf sirven para imprimier valores decimales
	// %f es para flotantes
	printf("ingreso al mes: %f\n",ingreso_mes);
	printf("ingreso al semana: %f\n",ingreso_semana);
}

