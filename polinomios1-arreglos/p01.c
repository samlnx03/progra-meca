#include <stdio.h>

void imprimir_poli(int poli[]);

int main(void)
{
	int i;
	int grado;
	int p[10];
	int expo;
	printf("polinomio (grado, cohef_n, cohef_n-1, .... cohe_0):");
	scanf("%d",&grado);
	p[0]=grado;
	for(i=1; i<=grado+1; i++){
		scanf("%d",&p[i]);
	}
	imprimir_poli(p);	
}

void imprimir_poli(int poli[]){
	int grado=poli[0];
	int i;
	for(i=1; i<=grado+1; i++){
		printf("%dX^%d ",poli[i], grado-i+1);
	}
}
