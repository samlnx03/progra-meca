#include <stdio.h>

int main()
{
	int x=5;
	int *ip;  // variable apuntador a entero
			// puede almacenar la direccion de cualquier dato entero
	ip=&x; 	// aqui tiene la direccion de x (que contiene un entero)

	printf("contenido de la direccion ip: %d\n", *ip);

	// ahora vamos a alterrar el valor guardo en la direccion de memoria
	// recordar que debe ser un entero

	*ip=10;
	printf("contenido de la direccion ip: %d\n", *ip); // 10
	// por supuesto que x ahora tiene el 10
	printf("contenido de x: %d\n", x); // 10
}

