#include <stdio.h>

int fun(int );
int fun2(int *);

int main()
{
	int z;
	int x=5;
	int *ip;  // variable apuntador a entero
			// puede almacenar la direccion de cualquier dato entero
	ip=&x; 	// aqui tiene la direccion de x (que contiene un entero)

	printf("contenido de la direccion ip: %d\n", *ip);
	printf("contenido de x: %d\n", x); // 10

	// ahora vamos a llamar una funcion con x pasado por valor
	//la funcion regresa el valor pasaso incrementado en 1 e intentara cambiar el valor recibido
	z=fun(x);
	// despues de llamar a fun imprimimos z y x
	printf("contenido de z:%d, x:%d\n", z,x); // 10

	//l lamar a fun2 con un apuntador
	z=fun2(&x);
	// despues de llamar a fun imprimimos z y x
	printf("contenido de z:%d, x:%d\n", z,x); // 10

}

// he aqui la funcion, tienes alguna duda en como va a funcionar?
//No, todo va bien
int fun(int n){
	int a=n+1;
	n=0;
	return a;
}

int fun2(int *n){
	int a=(*n)+1;  // recordar que *n me regresa lo almacenado en la direccion
	*n=0;
	return a;
}
