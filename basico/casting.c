#include <stdio.h>

int main(){
	int i=5,j=2,k;
	float v;
	k=i+j;
	printf("i=%d, j=%d, k=(i+j): %d\n",i,j,k);
	k=i/j;
	printf("i=%d, j=%d, k=(i/j): %d\n",i,j,k);
	v=i/j;
	printf("i=%d, j=%d, v=(i/j): %f\n",i,j,v);
	v=(float)(i/j);
	printf("i=%d, j=%d, v=(float)(i/j): %f\n",i,j,v);
	v=(float)i/j;
	printf("i=%d, j=%d, v=((float)i/j): %f\n",i,j,v);
	v=i/(float)j;
	printf("i=%d, j=%d, v=(i/(float)j): %f\n",i,j,v);
}

