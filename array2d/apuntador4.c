#include <stdio.h>
#include "fontdef.h"

void dibujar_letra(char definicion_asteriscos[],int r, int c);

#define CLEAR "\e[2J\e[H"
#define GOTORC "\e[%d;%df"
#define HIDECURSOR "\e[?25l"
#define SHOWCURSOR "\e[?25h"



int main(){
	char letrero[]="Hola";
	//FONT8x8[97][8]
	//char letrero[]={'H','o','l','a', ' ', 'm','u','n','d','o',0};
	char *c;

	char caracter_interes;
	int indice_en_arreglo;
	char *definicion_asteriscos;
	int j;
	int i=0;
	int COL=5;

	printf(CLEAR);
	//for(i=0; letrero[i]!='\0'; i++);
	//for(c=letrero; *c!='\0'; c++)i++;
	for(c=&letrero[0]; *c!='\0'; c++){
		printf("%c ",*c);
		caracter_interes=*c;
		indice_en_arreglo=caracter_interes - ' '+1;
		definicion_asteriscos=(char *)FONT8x8[indice_en_arreglo];
		for(j=0;j<8; j++)
			printf("%2x ",definicion_asteriscos[j]);
		printf("\n");
		
		dibujar_letra(definicion_asteriscos, 10, COL);
		i++;
		COL=COL+9;
	}


	printf("la longitud de la cadena letrero es  %d\n", i);
	
}


void dibujar_letra(char definicion_asteriscos[], int R, int C){
	int i, pix, j,k;
	int ncols=8;  // ancho de la letra
    for(i=0; i<8; i++){
		printf(GOTORC,R+i,C);
        pix=definicion_asteriscos[i];
        k=0x80;
        for(j=0; j<ncols; j++){
            if(pix & k)
                printf("*");
            else
                printf(" ");
            k=k>>1;
        }
        printf("\n");
    }

}
