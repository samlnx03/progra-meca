#include <stdio.h>
#include "fontdef.h"

void dibujar_letra(char definicion_asteriscos[]);


int main(){
	//char letrero[]="Hola mundo feliz";
	//FONT8x8[97][8]
	char letrero[]={'H','o','l','a', ' ', 'm','u','n','d','o',0};
	char *c;

	char caracter_interes;
	int indice_en_arreglo;
	char *definicion_asteriscos;
	int j;
	int i=0;
	//for(i=0; letrero[i]!='\0'; i++);
	//for(c=letrero; *c!='\0'; c++)i++;
	for(c=&letrero[0]; *c!='\0'; c++){
		printf("%c ",*c);
		caracter_interes=*c;
		indice_en_arreglo=caracter_interes - ' '+1;
		definicion_asteriscos=(char *)FONT8x8[indice_en_arreglo];
		for(j=0;j<8; j++)
			printf("%2x ",definicion_asteriscos[j]);
		printf("\n");
		dibujar_letra(definicion_asteriscos);
		i++;
	}


	printf("la longitud de la cadena letrero es  %d\n", i);
	
}


void dibujar_letra(char definicion_asteriscos[]){
	int i, pix, j,k;
	int ncols=8;  // ancho de la letra
    for(i=0; i<8; i++){
        pix=definicion_asteriscos[i];
        k=0x80;
        for(j=0; j<ncols; j++){
            if(pix & k)
                printf("*");
            else
                printf(" ");
            k=k>>1;
        }
        printf("\n");
    }

}
