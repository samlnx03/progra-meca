#include <stdio.h>
#include "fontdef.h"
// la pos 0 guarda las dimensiones
// la pos 1 es el espacio 0x20

// calcular el indice de un caracter C con  'C'-' '+1

#define CLEAR "\e[2J\e[H"
#define GOTORC "\e[%d;%df"
#define HIDECURSOR "\e[?25l"
#define SHOWCURSOR "\e[?25h"


void draw(unsigned char c[], int, int);

int main(){
	unsigned char *f;
	int l;
	char msg[]="Hola";
	char *c;
	// ubicar el mensaje en pantalla R=10, C=3
	int R=10, C=10;
	int fd=9;  // font desplazamiento (entre letra y letra)

	printf(CLEAR);
	// imprimir la letra en grande
	for(c=msg; *c!='\0'; c++){
		l=*c-' '+1;	
		f=(unsigned char *)FONT8x8[l];
		draw(f,R,C);
		C=C+fd;
	}
}

void draw(unsigned char c[], int R, int C){
	int i,j;
	unsigned char k;
	unsigned char pix;
	int ncols=7;
	for(i=0; i<8; i++){
		pix=c[i];
		k=0x80;
		printf(GOTORC,R,C);
		for(j=0; j<ncols; j++){
			if(pix & k)
				printf("*");
			else
				printf(" ");
			k=k>>1;
		}
		R++;
		printf("\n");
	}
}


