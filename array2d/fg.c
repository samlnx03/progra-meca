#include <stdio.h>

int main(){
	int letras[][8]={
		{0xf8, 0x80, 0x80, 0xf0, 0x80, 0x80, 0x80, 0x00},  // f
		{0x78, 0x88, 0x80, 0x80, 0x98, 0x88, 0x78, 0x00}
	};

	int i,j,l;
	unsigned char k;
	unsigned char pix;
	int ncols=7;
	printf("G: ");
	for(i=0; i<8; i++)
		printf("%x ",letras[1][i]);
	printf("\n");

	// imprimir la letra en grande
	for(l=0; l<2; l++){
	for(i=0; i<8; i++){
		pix=letras[l][i];
		k=0x80;
		for(j=0; j<ncols; j++){
			if(pix & k)
				printf("*");
			else
				printf(" ");
			k=k>>1;
		}
		printf("\n");
	}
	printf("\n");
	printf("\n");
	}
}

