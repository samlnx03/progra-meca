#include <stdio.h>
#include "fontdef.h"
// la pos 0 guarda las dimensiones
// la pos 1 es el espacio 0x20

// calcular el indice de un caracter C con  'C'-' '+1


void draw(unsigned char c[]);
int main(){
	unsigned char *f;
	int l;
	char msg[]="Hola mundo";
	char *c;

	// imprimir la letra en grande
	for(c=msg; *c!='\0'; c++){
		l=*c-' '+1;	
		f=(unsigned char *)MyFONT6x8[l];
		draw(f);
	}
}

void draw(unsigned char c[]){
	int i,j;
	unsigned char k;
	unsigned char pix;
	int ncols=7;
	for(i=0; i<8; i++){
		pix=c[i];
		k=0x80;
		for(j=0; j<ncols; j++){
			if(pix & k)
				printf("*");
			else
				printf(" ");
			k=k>>1;
		}
		printf("\n");
	}
	printf("\n");
	printf("\n");
}


