#include <stdio.h>

void imprimeC(int x, int y[][4]);

int main(){
	int datos[3][4]={
		{1, 2, 4, 5},
		{3, 2, 1, 0},
		{-4, 0, 2, -1} 
	};
	int nc;
	printf("que columna imprimo? ");
	scanf("%d",&nc);
	imprimeC(nc, datos);
	return 0;
}

void imprimeC(int c, int d[][4]){
	int i;
	for(i=0;i<3; i++){
		printf("%d\n",d[i][c]);
	}
}
