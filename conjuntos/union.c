#include <stdio.h>

#define MAXTAM 10

void imp(int C[],int n);
int agregar(int C[],int valor,int cardi);
int Union(int R[], int A[], int cA, int B[], int cB);


int main(void){
	
	int A[MAXTAM];
	int B[MAXTAM];
	int R[MAXTAM];
	int x;
	int cA=0, cB=0, cR=0;
	
	cA=agregar(A,2,cA);
	cA=agregar(A,2,cA);
	cA=agregar(A,3,cA);
	cA=agregar(A,6,cA);
	printf("Cardinalidad de A: %d\n", cA);
	imp(A,cA);

	cB=agregar(B,2,cB);
	cB=agregar(B,5,cB);
	cB=agregar(B,3,cB);
	cB=agregar(B,7,cB);
	printf("Cardinalidad de B: %d\n", cB);
	imp(B,cB);

	cR=Union(R,A,cA,B,cB);
	printf("Cardinalidad de R: %d\n", cR);
	imp(R,cR);
	
	//imp(A,cA);
	//imp(B,cB);
}

void imp(int C[],int n){

	int i;
	for(i=0;i<n;i++){printf("%d,", C[i]);}
	printf("\n");
}

int agregar(int C[],int valor,int pcardi){
	int i;
	if(pcardi<MAXTAM){
		for(i=0;i<(pcardi);i++){
			if(valor==C[i]){
				return pcardi;
			}
		}
		C[pcardi]=valor;
		pcardi=pcardi+1;	
		return pcardi;	
	}
	printf("Limite Exedido\n");
	return pcardi;
}


int Union(int R[], int A[], int cA, int B[], int cB){
	int cardi=0;
	int i;
	for(i=0; i<cA; i++){
		cardi=agregar(R,A[i],cardi);
	}
	// lo mismmo para B	
	for(i=0; i<cB; i++){
		cardi=agregar(R,B[i],cardi);
	}
	return cardi;
}

