#include <stdio.h>

int main(){
	int i,n;
	int u, p; // ultimo y penultimo de la serie finobacci
	int a; // actual es la suma de ultimo y penultimo
	p=0; u=1;  // inicio de la serie
	printf("cuantos terminos (n)? ");
	scanf("%d",&n);
	printf("%d, %d, ", p,u);
	for(i=1;i<=n; i++){
		a=u+p;
		printf("%d, ", a);
		p=u;
		u=a;
	}
	printf("\n");
	return 0;
}
