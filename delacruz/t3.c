// programa que cuenta desde un numero inicial hast un numero final
// contar de forma ascendente y de forma descendente

// ambos numero se piden al usuario al correr el programa

#include <stdio.h>

void contar(int i, int f, int direccion){
	// si direccion=1 conteo hacia arriba
	// si direccion=-1 contar hacia abajo
	int j;
	int condicion=1;

	for(j=i; condicion; j=j+direccion){
		printf("%d, ",j);
		if(direccion==1)
			condicion=(j<f);
		else
			condicion=(j>f);
	}
	printf("\n");
}



int main(){
	int n, i,vi,vf;
	printf("Dame el valor inicial: ");
	scanf("%d",&vi);
	printf("Dame el valor final: ");
	scanf("%d",&vf);
	printf("ok, imprimir desde %d hasta %d\n",vi, vf);
	// llamar  a la funcion
	contar(vi,vf,1); // hacia arriba
	contar(vf,vi,-1); // hacia abajo
}


