// imprimir la tabla de multiplcar de los numeros dados 
// entre uno inicial y uno final
// dados por el usuario

#include <stdio.h>

void tablamultiplicar(int i);

int main(){
	int n, i,vi,vf;
	printf("Dame el valor inicial: ");
	scanf("%d",&vi);
	printf("Dame el valor final: ");
	scanf("%d",&vf);
	printf("ok, tablas de multiplicar desde el %d hasta %d\n",vi, vf);

	for(i=vi; i<=vf; i++){
		tablamultiplicar(i);
		printf("\n");
	}
}


void tablamultiplicar(int j){
	int i;
	for(i=1; i<=10; i++){
		printf("%d x %d = %d\n",j,i,j*i);
	}
}

