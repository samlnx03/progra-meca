#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main(int argc, char const *argv[])

{
    char buf[20];
    fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);
    while(1){
	printf(". ");
    int numRead = read(0,buf,1);
    if(numRead > 0){
        printf("\nYou said: %s\n", buf);
		return;
    }
	}
}

