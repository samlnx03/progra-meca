#include <stdio.h>

// una union es una manera de ver al mismo valor almacenado desde diferentes
// puntos de vista
// se parece a una estructura, 
//    pero todo los miembros usan el mismo espacio de memoria
//    mientras que en la estructura cada miembro usa su propio espacio de mem
//    la memoria requerida queda determina por el elemento mas grande

union ic{
		int i;
		char c;
};
// int usa normalmente 4 bytes y char solo 1, el tamaño de la unios es 4

int main(){
	union ic u;
	printf("tamaño de int: %d\n",sizeof(int));
	printf("tamaño de char: %d\n",sizeof(char));
	printf("tamaño de u (union de int y char): %d\n",sizeof(u));
	u.i=0;  // limpiar basura en el entero (campo mas largo)
	u.c='B';
	printf("\nse almaceno una B en el char\n");
	printf("u visto como entero: %d (hex %04x)\n",u.i, u.i);
	printf("u visto como char: %c\n",u.c);
}

