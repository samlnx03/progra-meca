#include <stdio.h>

int main(int argc, char *argv[]){
	int minutos, resto;
	int m_1, m_08, m_07, m_05;   // minutos en cada tarifa
	
	minutos=atoi(argv[1]);
	resto=minutos-5;
	if(resto>0){
		m_1=5;
	} else {
		m_1=minutos;
		resto=0;
	}
	minutos=resto;
	resto=minutos-3;
	if(resto>0){
		m_08=3;
	} else {
		m_08=minutos;
		resto=0;
	}
	minutos=resto;
	resto=minutos-2;
	if(resto>0){
		m_07=2;
	} else {
		m_07=minutos;
		resto=0;
	}
	m_05=resto;
	printf("minutos a $1: %d\n",m_1);
	printf("minutos a $0.8: %d\n",m_08);
	printf("minutos a $0.7: %d\n",m_07);
	printf("minutos a $0.5: %d\n",m_05);
	// impuesto
	if(argv[2][0]=='f')
		printf("total= %f\n",(m_1*1+m_08*0.8+m_07*0.7+m_05*0.5)*1.03);
	else if(argv[2][0]=='m')
		printf("total= %f\n",(m_1*1+m_08*0.8+m_07*0.7+m_05*0.5)*1.16);
	else if(argv[2][0]=='v')
		printf("total= %f\n",(m_1*1+m_08*0.8+m_07*0.7+m_05*0.5)*1.1);
	else
		printf("eror en los datos\n");
	return 0;
}
