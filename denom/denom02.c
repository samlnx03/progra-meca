#include <stdio.h>

int main(void)
{
	int denom[]={1000,500,200,100,50,20,10,5,2,1}; // al compilar se define
													// el tamaño del arreglo
	int ingreso=15796;
	int i=0, d, Nbd;

	printf("Ingreso: "); scanf("%d",&ingreso);

	for (d=denom[0]; d>1;){
		Nbd=ingreso/d;  // num de billetes de la denominacion d
		if(Nbd>0){
				printf("Denominacion %d: %d\n",d,Nbd);
				ingreso=ingreso-Nbd*d;
		}
		i=i+1;
		d=denom[i];
	}
	if(ingreso)
		printf("Denominacion 1: %d\n",ingreso);
	
}

