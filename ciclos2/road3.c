#include <stdio.h>

/* 

road.c

dibuja una carretera con asteriscos a los lados
el ancho de la carretera es de ancho=10

hay nai=10 numero de asteriscos a la izquierda
y hay nad=10 numero de asteriscos a la derecha

se genera un numero aleatorio (-1 o 1) para sinuasidad de la carretera
agregando a los asteristcos de la izquierda y quitando a los de la derecha

*/

#include <stdlib.h>


int main(){
	int ancho=10;
	int nai=10;
	int nad=10;
	int j,i=0;	// 1000 veces
	int r;


	srand(time(0)); //Randomize seed initialization

	while(i<1000){
			r=rand() % 3 -1;
			if((nai+r)>0 && nai+r<20){
				nai=nai+r;
				nad=20-nai;
			}
			for(j=1; j<=nai; j++){
					printf("*");
			}
			for(j=1; j<=ancho; j++){
					printf(" ");
			}
			for(j=1; j<=nad; j++){
					printf("*");
			}
			printf("\n");
			i++;
	}
	return;
}

