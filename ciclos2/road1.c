#include <stdio.h>

/* 

road.c

dibuja una carretera con asteriscos a los lados
el ancho de la carretera es de ancho=10

hay nai=10 numero de asteriscos a la izquierda
y hay nad=10 numero de asteriscos a la derecha

*/

int main(){
	int ancho=10;
	int nai=10;
	int nad=10;
	int j,i=0;	// 1000 veces
	while(i<1000){
			for(j=1; j<=nai; j++){
					printf("*");
			}
			for(j=1; j<=ancho; j++){
					printf(" ");
			}
			for(j=1; j<=nad; j++){
					printf("*");
			}
			printf("\n");
			i++;
	}
	return;
}

