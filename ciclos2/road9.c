#include <stdio.h>

/* 

road.c

dibuja una carretera con asteriscos a los lados
el ancho de la carretera es de ancho=10

hay nai=10 numero de asteriscos a la izquierda
y hay nad=10 numero de asteriscos a la derecha

se genera un numero aleatorio (-1, 0 o 1) para sinuasidad de la carretera
agregando a los asteristcos de la izquierda y quitando a los de la derecha

*/

#include <stdlib.h>

// se usa un buffer para la pantalla de 25 renglones

#define CLEAR "\e[2J\e[H"
#define GOTORC "\e[%d;%df"
#define HIDECURSOR "\e[?25l"
#define SHOWCURSOR "\e[?25h"

int checa_colision(int ar, int ac,char *b[]){
	return 0;
}

void dibuja_auto(int pos, char *r){
		r[pos]='V';
		//intf(GOTORC,5+ar,15+ac); // auto row,col
		//intf("V");
}

void asigna(char v[][31], char *b[]){
	int i;
	for(i=0;i<25; i++){
		b[i]=&v[i][0];
	}	
}
char *desplaza(char *b[]){
	int i;
	char *p=b[0];
	// el reng 1 desaparece y es reemplazado por el 2
	// el 2 es reemplazado por el 3 y asi sucesivamente
	// el ultimo reng queda con lo del 1 que sera reescrito
	for(i=0;i<25-1; i++){
		b[i]=b[i+1];
		//printf("%s\n",b[i]);
	}
	b[i]=p;
	return p;
}

void llena(char *b, int nai, int ancho, int nad){
	int j,k;
	for(j=1; j<=nai; j++){
		b[j-1]='>';
	}
	k=j-1;
	for(j=1; j<=ancho; j++){
		b[k++]=' ';
	}
	for(j=1; j<=nad; j++){
		b[k++]='<';
	}
	b[30]=0;
}

void redibuja(char *b[]){
	int i;
	for(i=0;i<25; i++){
		printf(GOTORC,5+i,15);
		printf("%s",b[i]);
	}
}

void inicializa(char v[][31]){
	int i,j;
	for(i=0;i<25; i++){
		for(j=1; j<=10; j++){
				v[i][j-1]='*';
				v[i][j-1+10]=' ';
				v[i][j-1+20]='*';
		}
		v[i][30]=0;
	}
	for(i=0; i<25; i++) v[i][0]='A'+i;
}

int main(){
	char *buff[25];  // 25  apuntadores a char
	char video[25][31];	// pantalla a dibujar
	

	int ancho=10;
	int nai=10;
	int nad=10;
	int j,i=0;	// 1000 veces
	int r;
	char *p;

	int ar=0,ac=15; //coordenadas del auto reng,col

	printf(CLEAR);
	printf(HIDECURSOR);
	inicializa(video);
	asigna(video, buff);
	redibuja(buff);

	srand(time(0)); //Randomize seed initialization

	while(i<1000){
			r=rand() % 3 -1;
			if((nai+r)>0 && nai+r<20){
				nai=nai+r;
				nad=20-nai;
			}
			p=desplaza(buff);
			llena(p,nai,ancho,nad);

			dibuja_auto(ac,buff[ar]);
			redibuja(buff);

			checa_colision(ar,ac,buff);

			i++;
			usleep(500000);
			//sleep(5);
	}
	printf(SHOWCURSOR);
	return;
}

